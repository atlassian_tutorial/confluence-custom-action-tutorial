# Tutorial: Adding a Custom Action to Confluence

This plugin adds a custom action to Confluence. The custom action adds a "draft" label to a Confluence page accessible from the Tools drop-down menu. 

Find the tutorial for this source code here: [Adding a Custom Action to Confluence][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run
    
 [1]: https://developer.atlassian.com/x/kAIr
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project